/*
 * This file (net.pgrid.Message) is part of the PGrid project.
 *
 * Copyright (c) 2011. Vourlakis Nikolas. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package net.pgrid;

import java.nio.ByteBuffer;

/**
 * It represents a message to be sent to the pgrid network. This interface is
 * meant to be implemented by the client this library.
 *
 * @author Vourlakis Nikolas <nvourlakis@gmail.com>
 */
public interface Message {

    /**
     * Returns the type of this message.
     *
     * @return the type.
     */
    public int getType();

    /**
     * Returns the payload contained in this message. It is up to the client
     * how to deserialize it. The pgrid network knows nothing about its form.
     *
     * @return the payload.
     */
    public ByteBuffer getPayload();
}
