/*
 * This file (net.pgrid.PGridP2P) is part of the PGrid project.
 *
 * Copyright (c) 2011. Vourlakis Nikolas. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package net.pgrid;

import net.pgrid.event.DeliveryListener;
import net.pgrid.event.NotificationListener;
import net.pgrid.exception.RouteException;

/**
 * PGridP2P represents the pgrid peer-to-peer overlay network. It is
 * responsible for routing messages to a specified target key in the network.
 * When a message arrives, PGridP2P will deliver it to the application layer.
 * Some basic operations are also supported like joining and leaving the
 * network. Searching for specific peers and communicating directly to them.
 * Finally, it is possible to inform the application layer about specific
 * events that took place in the pgrid network.
 *
 * @author Vourlakis Nikolas <nvourlakis@gmail.com>
 */
public interface PGridP2P {

    /**
     * Initializes the pgrid p2p overlay network.
     */
    public void init();

    /**
     * Shutdowns the pgrid p2p overlay network.
     */
    public void shutdown();

    /**
     * Routes a message from the application layer to a Peer in the network
     * associated with the specified key.
     *
     * @param key     where the message will be routed to.
     * @param message to be sent.
     * @throws RouteException when the message cannot be routed to the key.
     */
    public void route(Key key, Message message) throws RouteException;

    /**
     * Routes a message from the application layer to all the peers in the network
     * associated with keys belonging in the given key range.
     *
     * @param keyRange the range where the message will be routed to.
     * @param message  to be sent.
     * @throws RouteException when the message cannot be routed to the keys.
     */
    public void route(KeyRange keyRange, Message message) throws RouteException;

    /**
     * Sends a message from the application layer to the given peer.
     *
     * @param peer    the destination of the message.
     * @param message to be sent.
     */
    public void send(Peer peer, Message message);

    /**
     * Given a key it will search the network for the peer associated with it.
     * This operation is synchronous.
     *
     * @param key     to look for.
     * @param timeout to wait for the answer.
     * @return the peer associated with.
     */
    public Peer lookup(Key key, long timeout);

    /**
     * Joins the pgrid network by contacting a bootstrap peer.
     *
     * @param peer the bootstrapper.
     */
    public void join(Peer peer);

    /**
     * Leaves the pgrid network.
     */
    public void leave();

    /**
     * Adds a {@link DeliveryListener} to receive messages from the pgrid
     * network and deliver them to the application layer.
     *
     * @param listener to be added.
     */
    public void addDeliveryListener(DeliveryListener listener);

    /**
     * Removes the specified listener if he is registered.
     *
     * @param listener to be removed.
     */
    public void removeDeliveryListener(DeliveryListener listener);

    /**
     * Adds a {@link NotificationListener} to deliver notifications to the
     * application layer about certain events triggered in the pgrid network,
     *
     * @param listener to be added.
     */
    public void addNotificationListener(NotificationListener listener);

    /**
     * Removes the specified listener if he is registered.
     *
     * @param listener to be removed.
     */
    public void removeNotificationListener(NotificationListener listener);
}
