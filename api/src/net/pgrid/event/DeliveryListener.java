/*
 * This file (net.pgrid.event.DeliveryListener) is part of the PGrid project.
 *
 * Copyright (c) 2011. Vourlakis Nikolas. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package net.pgrid.event;

import net.pgrid.Message;
import net.pgrid.Peer;

/**
 * A DeliveryListener registers at the pgrid overlay network. The latter is
 * responsible to deliver application messages send to him to the application
 * layer by using this listener. This interface is implemented by the client.
 *
 * @author Vourlakis Nikolas <nvourlakis@gmail.com>
 */
public interface DeliveryListener {

    /**
     * Delivers the received message to the application layer. Implementations
     * of this method can specify the way this message will be handled.
     *
     * @param sender  of the message.
     * @param message received by the overlay network.
     */
    public void deliver(Peer sender, Message message);
}
