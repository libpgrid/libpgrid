/*
 * This file (net.pgrid.event.NotificationListener) is part of the PGrid project.
 *
 * Copyright (c) 2011. Vourlakis Nikolas. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package net.pgrid.event;

/**
 * There are some events happening in the pgrid overlay network that may be
 * useful to the application layer. A NotificationListener notify the
 * application about these events. This interface is implemented by the client.
 *
 * @author Vourlakis Nikolas <nvourlakis@gmail.com>
 */
public interface NotificationListener {

    /**
     * The peer is connected to the pgrid network and ready to execute any
     * operations the application needs.
     */
    public void ready();

    /**
     * The peer has disconnected from the pgrid network.
     */
    public void disconnected();

    // TODO: Needs rework, maybe some more methods or create an event-listener mechanism
}
