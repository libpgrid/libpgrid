/*
 * This file (net.pgrid.exception.PGridException) is part of the PGrid project.
 *
 * Copyright (c) 2011. Vourlakis Nikolas. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package net.pgrid.exception;

/**
 * The class PGridException and its subclasses are exceptions that indicates
 * special conditions happening in the pgrid overlay network that the
 * application layer might want to catch.
 *
 * @author Vourlakis Nikolas <nvourlakis@gmail.com>
 */
public class PGridException extends Exception {

    /**
     * Constructs an Exception with no specified detail message.
     */
    public PGridException() {
        super();
    }

    /**
     * Constructs an Exception with the specified detail message.
     *
     * @param message the detail message.
     */
    public PGridException(String message) {
        super(message);
    }

    /**
     * Constructs a new exception with the specified detail message and cause.
     *
     * @param message message the detail message
     * @param cause   the cause (which is saved for later retrieval by the
     *                Throwable.getCause() method).
     *                (A null value is permitted, and indicates that the
     *                cause is nonexistent or unknown.)
     */
    public PGridException(String message, Throwable cause) {
        super(message, cause);
    }

    /**
     * Constructs a new exception with the specified detail message and cause.
     *
     * @param cause the cause (which is saved for later retrieval by the
     *              Throwable.getCause() method).
     *              (A null value is permitted, and indicates that the
     *              cause is nonexistent or unknown.)
     */
    public PGridException(Throwable cause) {
        super(cause);
    }
}
