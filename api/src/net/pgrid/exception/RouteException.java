/*
 * This file (net.pgrid.exception.RouteException) is part of the PGrid project.
 *
 * Copyright (c) 2011. Vourlakis Nikolas. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package net.pgrid.exception;

/**
 * This exception indicates that the pgrid overlay network failed to route a message
 * to a specific key.
 *
 * @author Vourlakis Nikolas <nvourlakis@gmail.com>
 */
public class RouteException extends PGridException {

    /**
     * Constructs an Exception with no specified detail message.
     */
    public RouteException() {
        super();
    }

    /**
     * Constructs an Exception with the specified detail message.
     *
     * @param message the detail message.
     */
    public RouteException(String message) {
        super(message);
    }

    /**
     * Constructs a new exception with the specified detail message and cause.
     *
     * @param message message the detail message
     * @param cause   the cause (which is saved for later retrieval by the
     *                Throwable.getCause() method).
     *                (A null value is permitted, and indicates that the
     *                cause is nonexistent or unknown.)
     */
    public RouteException(String message, Throwable cause) {
        super(message, cause);
    }

    /**
     * Constructs a new exception with the specified detail message and cause.
     *
     * @param cause the cause (which is saved for later retrieval by the
     *              Throwable.getCause() method).
     *              (A null value is permitted, and indicates that the
     *              cause is nonexistent or unknown.)
     */
    public RouteException(Throwable cause) {
        super(cause);
    }
}
