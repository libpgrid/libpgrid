/*
 * This file (net.pgrid.PGridP2PImpl) is part of the PGrid project.
 *
 * Copyright (c) 2011. Vourlakis Nikolas. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package net.pgrid;

import net.pgrid.event.DeliveryListener;
import net.pgrid.event.NotificationListener;
import net.pgrid.exception.RouteException;

/**
 * @author Vourlakis Nikolas <nvourlakis@gmail.com>
 */
public class PGridP2PImpl implements PGridP2P {

    @Override
    public void init() {
        // TODO: Implement init
    }

    @Override
    public void shutdown() {
        // TODO: Implement shutdown
    }

    @Override
    public void route(Key key, Message message) throws RouteException {
        // TODO: Implement route
    }

    @Override
    public void route(KeyRange keyRange, Message message) throws RouteException {
        // TODO: Implement route
    }

    @Override
    public void send(Peer peer, Message message) {
        // TODO: Implement send
    }

    @Override
    public Peer lookup(Key key, long timeout) {
        return null; // TODO: Implement lookup
    }

    @Override
    public void join(Peer peer) {
        // TODO: Implement join
    }

    @Override
    public void leave() {
        // TODO: Implement leave
    }

    @Override
    public void addDeliveryListener(DeliveryListener listener) {
        // TODO: Implement addDeliveryListener
    }

    @Override
    public void removeDeliveryListener(DeliveryListener listener) {
        // TODO: Implement removeDeliveryListener
    }

    @Override
    public void addNotificationListener(NotificationListener listener) {
        // TODO: Implement addNotificationListener
    }

    @Override
    public void removeNotificationListener(NotificationListener listener) {
        // TODO: Implement removeNotificationListener
    }
}
