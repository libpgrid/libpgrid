/*
 * This file (net.pgrid.PersistentRoutingTableXMLImpl) is part of the libpgrid project.
 *
 * Copyright (c) 2011. Vourlakis Nikolas. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package net.pgrid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Vourlakis Nikolas <nvourlakis@gmail.com>
 */
public class PersistentRoutingTableXMLImpl { //implements PersistentRoutingTable {
    
    private static final Logger logger_ = LoggerFactory.getLogger(PersistentRoutingTableXMLImpl.class);

    private static final String ROOT = "RoutingTable";
    private static final String HOST = "host";
    private static final String LEVEL = "level";
    private static final String ADDRESS = "address";
    private static final String PORT = "port";
    private static final String PATH = "path";
    private static final String UUID_STR = "uuid";
    private static final String TIMESTAMP = "timestamp";
//
//    @Override
//    public void load(String file, RT routingTable) throws FileNotFoundException {
//        // TODO: Break this methods into simpler (REFACTOR)
//        InputStream inStream = null;
//
//        try {
//            Map<Integer, PGridHost> map = new HashMap<Integer, PGridHost>();
//            XMLInputFactory inputFactory = XMLInputFactory.newInstance();
//            inStream = new FileInputStream(file);
//            XMLEventReader eventReader = inputFactory.createXMLEventReader(inStream);
//
//            PGridHost host;
//
//            int level = -1;
//            String address = null;
//            int port = -1;
//            String path = "";
//            String uuid = "";
//            long ts = -1;
//
//            while (eventReader.hasNext()) {
//                XMLEvent event = eventReader.nextEvent();
//
//                if (event.isStartElement()) {
//                    StartElement startElement = event.asStartElement();
//                    if (startElement.getName().getLocalPart().equals(HOST)) {
//                        Iterator attributes = startElement.getAttributes();
//                        while (attributes.hasNext()) {
//                            Attribute attribute = (Attribute) attributes.next();
//                            if (attribute.getName().toString().equals(LEVEL)) {
//                                level = Integer.parseInt(attribute.getValue());
//                            }
//
//                        }
//                    }
//
//                    if (event.isStartElement()) {
//                        if (event.asStartElement().getName().getLocalPart().equals(ADDRESS)) {
//                            event = eventReader.nextEvent();
//                            address = event.asCharacters().getData();
//                            continue;
//                        }
//                    }
//                    if (event.asStartElement().getName().getLocalPart().equals(PORT)) {
//                        event = eventReader.nextEvent();
//                        port = Integer.parseInt(event.asCharacters().getData());
//                        continue;
//                    }
//
//                    if (event.asStartElement().getName().getLocalPart()
//                            .equals(PATH)) {
//                        event = eventReader.nextEvent();
//                        path = event.asCharacters().getData();
//                        continue;
//                    }
//
//                    if (event.asStartElement().getName().getLocalPart()
//                            .equals(UUID_STR)) {
//                        event = eventReader.nextEvent();
//                        uuid = event.asCharacters().getData();
//                        continue;
//                    }
//
//                    if (event.asStartElement().getName().getLocalPart()
//                            .equals(TIMESTAMP)) {
//                        event = eventReader.nextEvent();
//                        ts = Integer.parseInt(event.asCharacters().getData());
//                        continue;
//                    }
//                }
//
//                if (event.isEndElement()) {
//                    EndElement endElement = event.asEndElement();
//                    if (endElement.getName().getLocalPart().equals(HOST)) {
//                        host = new PGridHost(address, port);
//                        host.setHostPath(path);
//                        host.setUUID(UUID.fromString(uuid));
//                        host.setTimestamp(ts);
//                        map.put(level, host);
//
//                        level = -1;
//                        address = null;
//                        port = -1;
//                        path = "";
//                        uuid = "";
//                        ts = -1;
//                    }
//                }
//            }
//            for (Map.Entry<Integer, PGridHost> entry : map.entrySet()) {
//                appendReference(entry.getValue());
//            }
//        } catch (UnknownHostException e) {
//            logger_.error("{}", e);
//        } catch (XMLStreamException e) {
//            logger_.error("{}", e);
//        } finally {
//            IOUtilities.closeQuietly(inStream);
//        }
//    }
//
//    @Override
//    public void store(String filename,RT routingTable) throws FileNotFoundException {
//        // TODO: Break this methods into simpler (REFACTOR)
//        FileOutputStream outStream = null;
//        try {
//            outStream = new FileOutputStream(filename);
//
//            XMLOutputFactory outputFactory = XMLOutputFactory.newInstance();
//            XMLEventWriter eventWriter = outputFactory
//                    .createXMLEventWriter(outStream);
//
//            XMLEventFactory eventFactory = XMLEventFactory.newInstance();
//            XMLEvent end = eventFactory.createDTD("\n");
//            XMLEvent tab = eventFactory.createDTD("\t");
//
//            StartDocument startDocument = eventFactory.createStartDocument();
//            eventWriter.add(startDocument);
//            eventWriter.add(end);
//
//            StartElement rtStartElement = eventFactory.createStartElement("",
//                    "", ROOT);
//            eventWriter.add(rtStartElement);
//            eventWriter.add(end);
//
//            String doubleTab = "\t\t";
//            int level = 0;
//            for (PGridHost host : getAllLevels()) {
//                StartElement hostStartElement = eventFactory.createStartElement("", "", HOST);
//                Attribute attribute = eventFactory.createAttribute(LEVEL, Integer.toString(level));
//                eventWriter.add(tab);
//                eventWriter.add(hostStartElement);
//                eventWriter.add(attribute);
//                eventWriter.add(end);
//
//                createNode(eventWriter, doubleTab, ADDRESS, host.getAddress().getHostName());
//                createNode(eventWriter, doubleTab, PORT, Integer.toString(host.getPort()));
//                createNode(eventWriter, doubleTab, PATH, host.getHostPath().toString());
//                createNode(eventWriter, doubleTab, TIMESTAMP, Long.toString(host.getTimestamp()));
//                createNode(eventWriter, doubleTab, UUID_STR, host.getUUID().toString());
//
//                eventWriter.add(tab);
//                eventWriter.add(eventFactory.createEndElement("", "", HOST));
//                eventWriter.add(end);
//                level++;
//            }
//
//            eventWriter.add(eventFactory.createEndElement("", "", ROOT));
//            eventWriter.add(end);
//            eventWriter.add(eventFactory.createEndDocument());
//            eventWriter.close();
//        } catch (XMLStreamException e) {
//            logger_.error("{}", e);
//        } finally {
//            IOUtilities.closeQuietly(outStream);
//        }
//    }
//
//    private void createNode(XMLEventWriter eventWriter, String indent, String name, String value)
//            throws XMLStreamException {
//        XMLEventFactory eventFactory = XMLEventFactory.newInstance();
//        XMLEvent end = eventFactory.createDTD("\n");
//        XMLEvent tab = eventFactory.createDTD(indent);
//        // Create Start node
//        StartElement sElement = eventFactory.createStartElement("", "", name);
//        eventWriter.add(tab);
//        eventWriter.add(sElement);
//        // Create Content
//        Characters characters = eventFactory.createCharacters(value);
//        eventWriter.add(characters);
//        // Create End node
//        EndElement eElement = eventFactory.createEndElement("", "", name);
//        eventWriter.add(eElement);
//        eventWriter.add(end);
//
//    }
}
