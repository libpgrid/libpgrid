/*
 * This file (net.pgrid.communication.NodeHandlerProxy) is part of the libpgrid project.
 *
 * Copyright (c) 2011. Vourlakis Nikolas. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package net.pgrid.communication;

import net.pgrid.Key;
import net.pgrid.Message;
import net.pgrid.PGridHost;
import net.pgrid.communication.corba.PeerReference;

/**
 * This class represents a proxy to the operations a *remote* node can execute.
 * 
 * @author Vourlakis Nikolas <nvourlakis@gmail.com>
 */
public class NodeHandleProxy {

    /**
     * Constructor.
     *
     * @param host the remote host that this proxy will represent.
     */
    public NodeHandleProxy(PGridHost host) {
        // TODO: Implement constructor
    }

    public void route(Key key, Message msg) {
        // TODO: Implement route
    }

    public void send(Message msg) {
        // TODO: Implement send
    }

    public PeerReference lookup(Key key, long timeout) {
        return null; // TODO: Implement lookup
    }

    public void exchange(PGridHost host/*, RoutingTable hostRT*/) {
        // TODO: Implement exchange
    }

    public void fixNode(PGridHost failed, String path) {
        // TODO: Implement fixNode
    }

    public void replace(PGridHost failed) {
        // TODO: Implement replace
    }

    public PGridHost getFailedPeer() {
        return null; // TODO: Implement getFailedPeer
    }

    public void registerForSolution() {
        // ask the peer this proxy represents to notify the local host when the
        // solution is ready
        // TODO: Implement addSolutionListener
    }

    public void solutionNotify(PGridHost replacer, PGridHost conjugate) {
        // notify the peer this proxy represents about the 2 hosts that repaired
        // the failed peer
        // TODO: Implement solutionNotify
    }
}
