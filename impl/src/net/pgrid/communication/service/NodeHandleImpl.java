/*
 * This file (net.pgrid.communication.service.NodeHandleImpl) is part of the libpgrid project.
 *
 * Copyright (c) 2011. Vourlakis Nikolas. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package net.pgrid.communication.service;

import net.pgrid.communication.corba.BasicMessage;
import net.pgrid.communication.corba.NodeHandlePOA;
import net.pgrid.communication.corba.PeerReference;
import net.pgrid.communication.corba.RoutedMessage;

/**
 * @author Vourlakis Nikolas <nvourlakis@gmail.com>
 */
public class NodeHandleImpl extends NodeHandlePOA {
    
    public NodeHandleImpl() {
        // TODO: Implement constructor
    }

    @Override
    public PeerReference peer() {
        // return info about this host
        return null; // TODO: Implement peer
    }

    @Override
    public PeerReference[] routingTable() {
        // return the routing table of this host
        return new PeerReference[0]; // TODO: Implement routingTable
    }

    @Override
    public String maintenanceState() {
        return null; // TODO: Implement maintenanceState
    }

    @Override
    public void route(RoutedMessage msg) {
        // TODO: Implement route
    }

    @Override
    public void send(BasicMessage msg) {
        // TODO: Implement send
    }

    @Override
    public PeerReference lookup(String key, long timeout) {
        return null; // TODO: Implement lookup
    }

    @Override
    public void updateInfo(PeerReference peer) {
        // TODO: Implement updateInfo
    }

    @Override
    public void exchange(PeerReference peer, PeerReference[] peerRT) {
        // TODO: Implement exchange
    }

    @Override
    public void fixNode(PeerReference failed, String path) {
        // TODO: Implement fixNode
    }

    @Override
    public void replace(PeerReference failed) {
        // TODO: Implement replace
    }

    @Override
    public PeerReference getFailedPeer() {
        return null; // TODO: Implement getFailedPeer
    }

    @Override
    public void addSolutionListener() {
        // TODO: Implement addSolutionListener
    }

    @Override
    public void solutionNotify(PeerReference replacer, PeerReference conjugate) {
        // TODO: Implement solutionNotify
    }
}
