/*
 * This file (net.pgrid.LoadSaveXMLRoutingTableTest) is part of the libpgrid project.
 *
 * Copyright (c) 2011. Vourlakis Nikolas. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package net.pgrid;


import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.FileNotFoundException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * @author Vourlakis Nikolas <nvourlakis@gmail.com>
 */
public class LoadSaveXMLRoutingTableTest {
    private static final String FILE = "test/src/net/pgrid/routingTable.xml";
    private static List<PGridHost> hostList_;

    @BeforeClass
    public static void beforeClass() throws UnknownHostException {
        PGridHost host0 = new PGridHost("localhost", 3000);
        host0.setHostPath("1");
        host0.setUUID(UUID.fromString("10d82159-3713-4b78-884e-4104720ae2d8"));
        host0.setTimestamp(1);

        PGridHost host1 = new PGridHost("localhost", 3001);
        host1.setHostPath("01");
        host0.setUUID(UUID.fromString("463dad77-627d-4a5d-930b-aab6a4523490"));
        host0.setTimestamp(2);

        PGridHost host2 = new PGridHost("localhost", 3002);
        host2.setHostPath("001");
        host0.setUUID(UUID.fromString("ac49991e-68a0-42ad-9c0e-ab328f193e4e"));
        host0.setTimestamp(3);

        PGridHost host3 = new PGridHost("localhost", 3003);
        host3.setHostPath("0011");
        host0.setUUID(UUID.fromString("5a29fac1-bb15-4cdd-a071-f33f2b847349"));
        host0.setTimestamp(4);

        hostList_ = new ArrayList<PGridHost>(4);
        hostList_.add(host0);
        hostList_.add(host1);
        hostList_.add(host2);
        hostList_.add(host3);
    }

    @AfterClass
    public static void afterClass() {
//        File file = new File(FILE);
//        file.delete();
    }

    @Test
    public void WhenStoring_ExpectResultedXMLFile() throws UnknownHostException {
        RoutingTable routingTable = new RoutingTable();
        for (PGridHost host : hostList_) {
            routingTable.appendReference(host);
        }

        try {
            routingTable.store(FILE);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void WhenLoadingExistingFile_ExpectResultedRoutingTable() {
        RoutingTable rt = new RoutingTable();
        try {
            rt.load(FILE);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        Assert.assertArrayEquals(hostList_.toArray(), rt.getAllLevels().toArray());

        for (PGridHost host : rt.getAllLevels()) {
            System.out.println(host +
                    " {" + host.getTimestamp() + "}" +
                    " [" + host.getHostPath() + "] " +
                    host.getUUID());
        }
    }
}


