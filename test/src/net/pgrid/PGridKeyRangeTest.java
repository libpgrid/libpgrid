/*
 * This file (net.pgrid.PGridKeyRangeTest) is part of the PGrid project.
 *
 * Copyright (c) 2011. Vourlakis Nikolas. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package net.pgrid;

import org.junit.Assert;
import org.junit.Test;

/**
 * @author Vourlakis Nikolas <nvourlakis@gmail.com>
 */
public class PGridKeyRangeTest {

    @Test(expected = NullPointerException.class)
    public void WhenNullKeys_ExpectNullPointerException() {
        PGridKeyRange keyRange =
                new PGridKeyRange(new PGridKey(""), new PGridKey(""));
        keyRange.contains(null);
    }

    @Test
    public void WhenNotNullKeys_ExpectResults() {
        PGridKeyRange keyRange =
                new PGridKeyRange(new PGridKey("00"), new PGridKey("00"));
        Assert.assertTrue(keyRange.contains(new PGridKey("001010101")));
    }

    @Test(expected = NullPointerException.class)
    public void WhenConstructingWithNullKeys_ExpectNullPointerException() {
        new PGridKeyRange(null, new PGridKey(""));
    }
}
